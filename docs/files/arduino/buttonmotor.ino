// Moïra Vanderslagmolen 8 december 2023
// License : Creative Commons Attribution-ShareAlike 4.0 International CC BY-SA 4.0

#include <Servo.h>

// pushbutton pin
const int buttonPin = 8;

// servo pin
const int servoPin = 9;
Servo servo;

// create a variable to store a counter and set it to 0
int counter = 0;

byte lastButtonState = LOW;
byte ledState = LOW;
bool servo_on = false;

void setup()
{
    servo.attach(servoPin);

    // Set up the pushbutton pins to be an input:
    pinMode(buttonPin, INPUT);
}

void loop()
{
    int buttonState = digitalRead(buttonPin);
    if (buttonState != lastButtonState)
    {
        lastButtonState = buttonState;
        if (buttonState == LOW)
        {
            if (servo_on == true)
            {
                servo_on = false;
                delay(400);
            }
            else if (servo_on == false)
            {
                delay(400);
                servo_on = true;
            }
        }
    }
    if (servo_on == true)
    {
        counter++;
        if (counter == 0)
            servo.write(20);
        else if (counter == 1)
            servo.write(90);
        else if (counter == 2)
            servo.write(150);
        else if (counter == 3)
            servo.write(180);
        else
            counter = 0;
    }
}
