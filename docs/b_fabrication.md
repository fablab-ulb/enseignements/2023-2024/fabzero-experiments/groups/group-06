# Textile manufacturing

To address the issue of unrecycled fabric, we needed to understand what it is made of and how. Indeed, the type of textile used or the way the fabric is sewed (or any other technique) will most likely determine what can be done with it afterwards.

## Types of textile

The main distinction we can already make is between natural and synthetic fibers. You will find here below some examples of both these types.

### Natural fibers

Natural fibers are made from plants, animals, or minerals. They are biodegradable and have a variety of properties.

* **Cotton** is a soft, absorbent fiber made from the cotton plant. It is one of the most popular fibers in the world.


* **Wool** is a soft, warm fiber made from sheep's fleece. It is a natural insulator and is used to make a variety of clothing and fabrics.  


* **Silk** is a luxurious, smooth fiber made from silkworms. It is known for its drape and luster.


|Cotton|Wool|Silk|
|---|----|----|
|<img src="../images/fiber/cotton.jpeg" alt="drawing" width="200"/>|<img src="../images/fiber/wool.jpeg" alt="drawing" width="200"/>|<img src="../images/fiber/silk.jpeg" alt="drawing" width="200"/> |

* **Linen** is a strong, durable fiber made from flax plants. It is known for its crispness and breathability.

* **Hemp** is a strong, durable fiber made from hemp plants. It is a sustainable and eco-friendly fiber.

* **Jute** is a strong, coarse fiber made from jute plants. It is used to make sacks, bags, and rugs.


|Linen|Hemp|Jute|
|---|----|----|
|<img src="../images/fiber/lien.jpeg" alt="drawing" width="200"/>|<img src="../images/fiber/hemp.jpeg" alt="drawing" width="200"/>|<img src="../images/fiber/jute.jpeg" alt="drawing" width="200"/> |
  
### Synthetic fibers

Synthetic fibers are made from chemicals derived from petroleum or natural gas. They are not biodegradable and have a variety of properties.

* **Polyester** is a strong, durable fiber that is wrinkle-resistant and quick-drying. It is one of the most popular synthetic fibers.

* **Nylon** is a strong, elastic fiber that is used to make a variety of clothing, including hosiery, sportswear, and outerwear.

* **Acrylic** is a soft, warm fiber that is often used to make sweaters and other knitwear.

|Polyester|Nylon|Acrylic|
|----|---|---|
|<img src="../images/fiber/polyester.jpg" alt="drawing" width="200"/>|<img src="../images/fiber/nylon.jpeg" alt="drawing" width="200"/>| <img src="../images/fiber/acrilic.jpeg" alt="drawing" width="200"/>|

* **Olefin** is a strong, lightweight fiber that is used to make swimwear, tents, and other outdoor gear.


* **Spandex** is a stretchy fiber that is often used in sportswear and lingerie.

|Olefin|Spandex|
|----|---|
|<img src="../images/fiber/olefin.jpeg" alt="drawing" width="200"/>|<img src="../images/fiber/spandex.jpeg" alt="drawing" width="200"/>|

## Manufacturing

### First part: from fiber to yarn

Beginning with the raw material, the first big process will be transforming that material into yarn. From there, it then will be possible to create fabric. However, this first process is already quite complex and long, as well as depending on the raw material. We will try to briefly describe the main steps for cotton, wool and silk (all three are natural fibers), as well as synthetic fibers. 
  
There exist numerous natural fibers in addition to the three we introduced. The sources are varied such as hemp, bamboo and eucalyptus. In general, the purpose will be to transform them in a way allowing to extrude[^1] the cellulose from the materials. 

#### Cotton

##### 1. Cultivating and Harvesting

The cotton is harvested by cotton pickers and stripper harvesters, who pluck the ears of grain without winnowing[^2]. After, the cotton is processed by a cotton engine, which removes the dirt, leaves and stems.

##### 2. Preparatory Processes

The bale of cotton is broken by an opener[^3]. To loosen the cotton up, the cotton is beaten by a beater bar. To remove the vegetable matter, the cotton is fed through multiple rollers. Then, the carding[^4] process is to separate the fibers and assemble them into a loose strand. The final result of the carding process is a large rope of fibers.

##### 3. Spinning

The spinning process transforms the fiber into a yarn. The fiber is put into a rotating drum, where it attaches itself to the fiber being formed. After, the plying process takes part. The yarn is pulled by multiple bobbins and twisted to create a bigger yarn.

#### Wool

##### 1. Harvesting

The wool is taken from the sheep and brought to the mill.

##### 2. Classification

There are two types of wool : long wool goes through "worsted" processing, and shorter wool goes through woolen processing.

##### 3. Scouring

We wash the wool to remove impurities. Plants and branches are removed using sulfuric acid.

##### 4. Carding

For woolen processing, the carding wool is split into slabs. For worsted processing, the carding wool is split into slivers.

##### 5. Spinning:

During spinning, worsted and woolen are twisted to bind the fibers into a strong yarn.

#### Silk

Silk comes from the cocoons of silkworms. It must first be harvested. After that, the cocoon is put in hot water and sorted by hand to obtain the silk. It is then sorted by color, size and quality. The silk is dried and put into a machine to create the yarn.

#### Synthetic fibers

Synthetic fibers are created by chemical synthesis. For instance, polyester is created by first mixing ethylene glycol with dimethyl terephthalate at high heat, which gives us a monomer. The monomer is remixed with dimethyl terephthalate which gives us a polymer. Next, the polymer is extruded by a spinneret to create fibers and spun into a yard.  

### Second part : from yarn to fabric

Once the yarn is obtained, it can now be used to create fabric. Different techniques exist.  
There are three main types of fabric: (a) woven, (b) knit and (c) non-woven.
  
<img src="../images/fiber/types-of-common-fabric.png" width="400"/>

#### Knit

Knitting is a method of creating fabric by interlocking loops of yarn with needles. It involves pulling loops of yarn through existing loops to form a series of interconnected rows. Knitting can be handmade, individuals use needles to create garments or items, or machine-made, which involves automated machines for mass production. Knitting allows for a wide range of patterns and textures, making it a popular craft for creating clothing, accessories, and decorative items.

There are two types of knit : weft, the most common, where the wales[^5] are perpendicular to the course of the yarn, using a single yarn for the hole fabric ; and warp where the wales and courses run roughly parallel, and each wale corresponds to a different yarn.

<img src="../images/fiber/weft_warp.png" width="200"/>

Both weft and warp can be then classified in subcaterogies and variants.

##### Weft knitting

It is the usual type of handmade knitting. It includes the basics techniques: your plain (Jersey) or garter knit, purl knit, knit-purl, and rib knit.

<img src="../images/fiber/weft_basic_stitches.png" width="200"/>

(a) Knit stitches  
(b) Purl stitches  
(c) Rib fabric  
(d) Garter fabric

###### Purl knit

Purl knitting is a type of weft knitting. In the purl stitch, loops are drawn to opposite sides of the fabric, which, on both sides, has the appearance of the back of a plain stitch fabric.

###### Jersey

Jersey, or plain fabric, is a weft knit fabric that is knitted on a single set of needles with all loops meshing in the same direction. By and large, it is knitted in plain stitch. On the other hand, the double jersey is knit using two sets of needles, does not curl at the edges (when cut) and has a more stable structure.

<img src="../images/fiber/jersey.png" width="300"/>

###### Interlock knit

Interlock knit fabrics are a variation of rib knit construction in the weft fabric. The nature of this construction allows interlock knits to have a built-in stretch and drape really well on the body. The front and back of the fabric are identical, creating the impression that the fabric is comprised of two layers, though they can’t be separated. This is why it’s categorized as a double-knit fabric.

You can tell this fabric apart from regular stretch knit fabrics by the rows, or ribs, of a ‘V’ shape down the face and back of the fabric. Essentially, the two sides are knitted together at the same time on one machine and remain interlocked to be thicker than other knit fabrics.

<img src="../images/fiber/interlock.jpeg" height="100"/>

##### Warp knitting

###### Tricot

Tricot is a type of thin-knit fabric. It's made on a flatbed knitting device (which makes it different from the other knitted fabrics) and has a crosswise rib on its rear and a noticeable zigzag look on its front side. Tricot can be made from both natural and synthetic fibers, which give it its smooth and elastic properties. It's known for its unique strength and feel, which makes it easier to work with compared to other knit materials. Tricot is as strong and durable as woven fabric, but it retains the elasticity of knit material. Among tricot you can also find subcategories (single, double-bar, ...).

<img src="../images/fiber/tricot.png" height="200"/>

###### Raschel

Raschel knit is a type of warp-knitted fabric. It's made on a special knitting machine by taking threads from the warp beam. Raschel knits have a lacelike, open construction, with a heavy, textured yarn held in place by a much finer yarn. They can be made in a variety of types, ranging from fragile to coarse, and usually have limited stretch. Most raschel knits have complicated patterns that look like crochet or lace with open spaces and a surface effect that looks almost like a three-dimensional model. The knitting method known as raschel knitting is done using industrial knitting machines and not by hand.

<img src="../images/fiber/raschel.jpg" height="250"/>

###### Milanese

Milanese knitting is a type of warp knit fabric structure where the stitches are made by two loops, each from different warp yarns. The yarn of the loops runs diagonally, alternating from one stitch to another in each course. This results in a diamond effect on the back of the fabric and a fine rib showing on the surface. Smooth-textured fine yarns are used in these knits in limited design patterns. Milanese knits are smoother, more regular in structure, have greater elongation, and are higher in bursting strength than tricot knits.

<img src="../images/fiber/milanese_structure.png" height="250"/>

#### Woven

There are two main types of mesh fabric weaving: plain weave and twill weave. Plain weave is the simplest type of mesh fabric weave, and it is characterized by a regular pattern of square or rectangular openings. Twill weave is more complex than plain weave, and it is characterized by a diagonal pattern of openings.

##### Plain weave

It is the simplest type of mesh fabric weave. It is characterized by a regular pattern of square or rectangular openings. The warp yarns are passed over and under the weft yarns in a straight line, creating a consistent pattern.

<img src="../images/fiber/plain_weave_mesh_fabric.jpeg" height=200/>

Plain weave is the most common type of mesh fabric weave for clothing because it is easy to produce and produces a strong, lightweight fabric. It is also commonly used for window screens, insect netting, and other applications where a uniform mesh is needed.

##### Twill weave

It is a more complex type of mesh fabric weave than plain weave. It is characterized by a diagonal pattern of openings. The warp yarns are passed over and under the weft yarns in a zig-zag pattern, creating a textured and more durable fabric.

<img src="../images/fiber/twill_weave_mesh_fabric.jpeg" height="200"/>

Twill weave is often used for upholstery, luggage, and other applications where a durable and textured fabric is needed. It is also commonly used for military uniforms because it is strong and resists tearing.

Here is a table summarizing the key differences between plain weave and twill weave:

| Feature | Plain weave | Twill weave |
|---|---|---|
| Pattern | Regular, square or rectangular openings | Diagonal pattern of openings |
| Appearance | Smooth, uniform | Textural, more durable |
| Durability | Moderate | High |
| Common uses | Clothing, window screens, insect netting | Upholstery, luggage, military uniforms |

#### Non-Woven

<img src="../images/fiber/nonwoven.jpeg" height="200"/>

Non-woven is made from short and long fibers bonded together. Non-woven is neither woven nor knitted, the fibers are stacked or mixed together and don't have a repetitive pattern. The best example of a non-woven fabric is felt, as you can see here above. It is used to create hats and coats.
  
## Vocabulary
  
[^1]: Extrusion: Mechanic process of transforming a material into a thread
[^2]: Winnowing: Separating the grain from the chaff, which is the protecting case of the seed
[^3]: Opener: Machine with large spikes
[^4]: Carding: Mechanical process that disentangles, cleans and intermixes fibers to produce a continuous web or sliver suitable for subsequent processing
[^5]: Wale: Column of loops running lengthwise