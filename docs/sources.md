# Sources

Here is a list of sources we used to develop this project :

* [BOSFORUS textile : Milano Knit Fabric](http://bosforustextile.com/milano_knit_fabric.html)
* [FabricLore : Knitting](https://fabriclore.com/blogs/journal/knit-fabrics-and-their-uses)
* [FabricLore : Raschel Knit Fabric](https://fabriclore.com/blogs/fabric-wiki/information-about-raschel-knit)
* [Textiles](https://www.res-sources.be/fr/textiles/)
* [Textile Apex : Synthetic Fibre Production Process](https://textileapex.com/synthetic-fibre-production-process/)
* [Waste Shredder Machine](https://www.semanticscholar.org/paper/DESIGNING-TEXTILE-WASTE-SHREDDER-MACHINE-FOR-Afifuddin-Harianto/6f41beb0194ad7d301dff3c78314d9df60638033)
* [Wikipedia : Textile Manufacturing](https://en.wikipedia.org/wiki/Textile_manufacturing)
* [Wikipedia : Warp and weft knitting](https://en.wikipedia.org/wiki/Warp_and_weft)
* [YouTube : Duletai New Material - Production of Polyester Yarn](https://www.youtube.com/watch?v=uFJl7ydYb2I)
* [YouTube : The Woolmark Company - Wool Production](https://www.youtube.com/watch?v=YwRbyTCqOQY)
* [YouTube : Noal Farm - Silk Production](https://www.youtube.com/watch?v=vlkIPELbKdo)