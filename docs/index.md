# Group and project presentation




## About our team

Our team is composed of :

|Members|Studies|
|-------|-------|
|[Sif Eddine Boughris](https://sif-boughris-fablab-ulb-enseignements-2023-2024--7dd51b009114d3.gitlab.io/) | Last year in computer science and engineering|
[Louis Jonas](https://louis-jonas-fablab-ulb-enseignements-2023-2024-f-bd71ce6a6b7cd5.gitlab.io/) | Third year bioengineering
[Alishba Naqi](https://alishba-naqi-fablab-ulb-enseignements-2023-2024--51203977958c85.gitlab.io/)| Third year bioengineering
[Moïra Vanderslagmolen](https://moira-vanderslagmolen-fablab-ulb-enseignements-2-810aa5493f4f56.gitlab.io/) | Third year computer science

  
We are a multidisciplinary and collaborative unit working on a project within the framework of the [ULB - FabZero Experiments](https://class-website-fablab-ulb-enseignements-2023-2024-c4a40531a3337f.gitlab.io/). This team consists of students from diverse academic backgrounds, including computer science, bioengineering, and computer engineering. This diversity in skill sets and knowledge brings a wealth of perspectives and expertise to the table, making our group well-equipped to tackle a wide range of challenges and come up with innovative solutions. Together, we harnessed digital fabrication techniques and creative approaches to design experiments, demonstrating the power of teamwork and cross-disciplinary cooperation.

## Our Project abstract

Our project explores the world of textiles. We noticed that many people tend to throw away torn or damaged clothes. So, we decided to figure out smart ways to reuse these fabrics and cut down on unnecessary waste.

**In Belgium, 49 percent of textiles get burned each year**, and that's not so great for the environment. Our goal is to stop this wasteful practice. We want to find better ways to use old clothes and encourage everyone to be more thoughtful about how we handle our textiles. By doing this, we hope to make a positive impact on how we approach clothing waste and contribute to a greener way of doing things. 
