// Moïra Vanderslagmolen 8 december 2023
// License : Creative Commons Attribution-ShareAlike 4.0 International CC BY-SA 4.0

const int buttonPin = 2;
const int ledPin = 1;

byte lastButtonState = LOW;
byte ledState = LOW;

void setup()
{
    pinMode(ledPin, OUTPUT);
    pinMode(buttonPin, INPUT);
}

void loop()
{
    byte buttonState = digitalRead(buttonPin);
    if (buttonState != lastButtonState)
    {
        lastButtonState = buttonState;
        if (buttonState == LOW)
        {
            ledState = (ledState == HIGH) ? LOW : HIGH;
            digitalWrite(ledPin, ledState);
        }
    }
}
